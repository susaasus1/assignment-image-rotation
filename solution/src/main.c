#include "../include/bmp_util.h"
#include "../include/file_util.h"
#include "../include/transform.h"

int main(int argc, char **argv) {
    bool in,out;
    char *path_input_file = NULL, *path_output_file = NULL;
    struct image img;
    struct image new_image;
    if (argc<3) {
        fprintf(stderr, "Вы указали не верное количество аргументов\nВы должны указать путь к исходному файлу и путь к конечному файлу!\n");
        return 0;
    } else {
    	path_input_file = argv[1];
    	path_output_file= argv[2];
    }
    FILE *input_file = NULL, *output_file = NULL;
    in = open_file(&input_file, path_input_file, "rb");
    out = open_file(&output_file, path_output_file, "wb");
    if (in || out) {
        fprintf(stderr, "Невозможно прочитать одинз из файлов\n");
        return 0;
    }
    if (from_bmp(input_file, &img)){
        fprintf(stderr, "Невозможно скновертировать данный файл в image, возможно ваш файл не BMP\n");
        return 0;
    } else{
    	fprintf(stdout, "Ваш файл успешно сконвертировался\n");
    }
    new_image = rotate_left_90_degree(img);
    if (to_bmp(output_file, &new_image)){
        fprintf(stderr, "Невозможно сконвертировать изображение в BMP\n");
        return 0;
    }else {
    	fprintf(stdout, "Картинка успешно сконвертирована в BMP\n");
    }

    in = close_file(&input_file);
    out = close_file(&output_file);
    if (in || out){
        fprintf(stderr, "Невозможно закрыть файлы\n");
        return 0;
    }
    image_free_memory(&new_image);
    image_free_memory(&img);
    return 0;
}

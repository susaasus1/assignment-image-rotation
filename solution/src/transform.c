//
// Created by Daniil on 24.11.2021.
//
#include "../include/transform.h"

#include <malloc.h>

struct image rotate_left_90_degree(struct image const img){
    struct pixel *pixels = malloc(sizeof(struct pixel) * img.height * img.width);

    for (size_t i = 0; i < img.height; i++ ) {
        for (size_t j = 0; j < img.width; j++) {
            *(pixels + j * img.height + (img.height - 1 - i)) = *(img.data + i * img.width + j);
        }
    }
    struct image img1;
    img1.width = img.height;
    img1.height = img.width;
    img1.data = pixels;
    return img1;
}


#include "../include/image.h"

bool image_malloc_data(struct image *img){
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    return img->data == NULL;
}

void image_free_memory(struct image *img){
    free(img->data);
}

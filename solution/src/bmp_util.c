//
// Created by Daniil on 24.11.2021.
//

#include "../include/bmp_util.h"

#include <malloc.h>


static uint32_t SIGNATURE = 19778;
static uint32_t RESERVED = 0;
static uint32_t HEADER_SIZE = 40;
static uint16_t PLANES = 1;
static uint32_t COMPRESSION = 0;
static uint32_t PIXEL_PER_M = 2834;
static uint32_t COLORS_USED = 0;
static uint32_t COLORS_IMPORTANT = 0;
static size_t BIT_COUNT = 24;

static enum read_status read_bmp_header(FILE *file_in, struct bmp_header *header);
static enum read_status read_pixels(FILE *file_in, struct image *img);
static enum write_status create_header(struct image const *img, struct bmp_header *header);
static size_t calculate_padding(size_t width);
enum read_status from_bmp(FILE *file_in, struct image *img) {
    if (!file_in) {
    	return READ_NULL; 
    }
    if (!img) {
    	return READ_NULL; 
    }
    struct bmp_header header={0};
    enum read_status header_status = read_bmp_header(file_in, &header);
    if (header_status) { 
    	return header_status;
    }
    img->height = header.height;
    img->width = header.width;
    if(fseek(file_in, header.data_offset, SEEK_SET)!=0){
    	return READ_INVALID_HEADER;
    }
    return read_pixels(file_in, img);
}

enum write_status to_bmp(FILE *file_out, struct image *img) {
    if ((!file_out)!=WRITE_OK) {
    	return WRITE_NULL;
    }
    if ((!img)!=WRITE_OK) {
    	return WRITE_NULL;
    }
    struct bmp_header header={0};
    create_header(img, &header);
    size_t count =fwrite(&header, sizeof(struct bmp_header), 1, file_out);
    if (count!=1){
    	return WRITE_ERROR;
    }
    if (fseek(file_out, header.data_offset, SEEK_SET)!=0){
    	return WRITE_NULL;
    }
    size_t padding = calculate_padding(img->width);
    
    size_t *line_padding = malloc(4);
    if (!line_padding) {
    	return WRITE_ERROR; 
    }
    for (size_t i = 0; i < padding; i++){
        *(line_padding + i) = 0;
    }
    if (img->data != NULL){
        for (size_t i = 0; i < img->height; ++i) {
            size_t count1=fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, file_out);
            size_t count2=fwrite(line_padding, padding, 1, file_out);
            if(count1!=1 && count2!=1){
                free(line_padding);
            	return WRITE_ERROR;
            }
        }
    }
    free(line_padding);
    return WRITE_OK;
}


static enum read_status read_bmp_header(FILE *file_in, struct bmp_header *header) {
    if(fseek(file_in, 0, SEEK_END)!=0){
    	return READ_INVALID_HEADER;
    }
    size_t f_size = ftell(file_in);
    if (f_size < sizeof(struct bmp_header)) {
    	return READ_INVALID_HEADER;
    }
    rewind(file_in);
    size_t result=fread(header, sizeof(struct bmp_header), 1, file_in);
    if(result!=1){
    	return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status read_pixels(FILE *file_in, struct image *img) {
    if (image_malloc_data(img)) {
    	return READ_ERROR; 
    }
    size_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        size_t res=fread(img->data + i * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, file_in);
        if(res!=1){
        	return READ_NULL;
        }
        if(fseek(file_in, padding, SEEK_CUR)!=0){
        	return READ_NULL;
        }
    }
    return READ_OK;
}

static enum write_status create_header(struct image const *img, struct bmp_header *header) {
    size_t padding = calculate_padding(img->width);
    header->signature = SIGNATURE;
    header->image_size = (img->width * sizeof(struct pixel) + padding) * img->height;
    header->filesize = header->image_size + sizeof(struct bmp_header);
    header->reserved = RESERVED;
    header->data_offset = sizeof(struct bmp_header);
    header->size = HEADER_SIZE;
    header->width = img->width;
    header->height = img->height;
    header->planes = PLANES;
    header->bit_count = BIT_COUNT;
    header->compression = COMPRESSION;
    header->x_pixels_per_m = PIXEL_PER_M;
    header->y_pixels_per_m = PIXEL_PER_M;
    header->colors_used = COLORS_USED;
    header->colors_important = COLORS_IMPORTANT;
    return WRITE_OK;
}
static size_t calculate_padding(size_t width){
	size_t padding= 4-width*sizeof(struct pixel)%4;
	if (padding==4){
		padding=0;
	}
	return padding;
}

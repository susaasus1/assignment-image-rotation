//
// Created by Daniil on 24.11.2021.
//

#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel *data;
};
#pragma pack(pop)
struct pixel {
    uint8_t b, g, r;
};

bool image_malloc_data(struct image *img);
void image_free_memory(struct image *img);

#endif //LAB3_IMAGE_H

//
// Created by Daniil on 24.11.2021.
//

#ifndef LAB3_TRANSFORM_H
#define LAB3_TRANSFORM_H

#include "../include/image.h"

struct image rotate_left_90_degree(struct image const img);

#endif //LAB3_TRANSFORM_H
